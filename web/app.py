from flask import Flask, request, redirect, url_for
app=Flask(__name__)
app.debug=True
app.config['BOOTSTRAP_SERVE_LOCAL']=True

from flask.ext.bootstrap import Bootstrap
Bootstrap(app)

from flask.ext.script import Manager
manager=Manager(app)


import os.path
def mkpath(p):
	return os.path.normpath(
		os.path.join(
			os.path.dirname(__file__),
			p))


from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = (
	'sqlite:///'+mkpath('../agarIA.db'))
db=SQLAlchemy(app)
db.session.execute('pragma foreign_keys=ON') # peut etre specifique à sqlite

app.config['SECRET_KEY']="f5e63d01-f3a7-48d1-af81-bb0c3f3b458a"

from flask.ext.login import LoginManager
login_manager=LoginManager(app)

import os
from werkzeug import secure_filename

UPLOAD_FOLDER_image = '/home/elghonnaji/' # répertoire ou seront uploader les fichiers
UPLOAD_FOLDER = './programme/' # répertoire ou seront uploader les fichiers
ALLOWED_EXTENSIONS = set(['py']) # Ensemble des extensions de fichiers autorisés
ALLOWED_EXTENSIONS_image = set(['png', 'jpg', 'jpeg', 'gif'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['UPLOAD_FOLDER_image'] = UPLOAD_FOLDER_image
