drop table IF EXISTS Jeu;
drop table IF EXISTS Tournoi;
drop table IF EXISTS Participer;
drop table IF EXISTS Programme;
drop table IF EXISTS Programmer;
drop table IF EXISTS Utilisateur;
drop table IF EXISTS Compte_Utilisateur;
drop table IF EXISTS Compte;

create table Compte(
Id_compte Integer(4) AUTO_INCREMENT,
Id_util Integer(4), 
Mdp_compte VarChar(20),  
Identifiant_comtpe VarChar(20),
Type_compte Varchar(20), -- admin/ utilisateur
PRIMARY KEY (Id_compte)
);

create table Utilisateur(
Id_util Integer(4) ,
Prenom_util VarChar(20),
Nom_util VarChar(20),
Email_util VarChar(50),
PRIMARY KEY (Id_util)
);

create table Programmer(
Id_programme Integer(4),
Id_util Integer(4)
);

create table Programme(
Id_programme Integer(4) AUTO_INCREMENT,
contenu_programme VarChar(100),
-- demander comment on pourrais sauvegarder un ficher car programme risque detre long
PRIMARY KEY (Id_programme)
);

create table Participer(
Id_tournoi Integer(4),
Id_programme Integer(4),
Score Integer(6), --demander de quelle taille pourra etre le score
Num_classement Integer(3),
Actif VarChar(3) --savoir si le joueur est tj dans le tournoi oui/non
);

create table Tournoi(
Id_tournoi Integer(4) AUTO_INCREMENT,
Date_debut date,
Date_fin date,
Id_jeu Integer(4),
PRIMARY KEY (Id_tournoi)
);

create table Jeu(
Id_jeu Integer(4) AUTO_INCREMENT,
Nom_jeu VarChar(20),
Regle_jeu VarChar(100), --demander si doit etre dans fichiers txt vu sa longueur
PRIMARY KEY (Id_jeu)
);
